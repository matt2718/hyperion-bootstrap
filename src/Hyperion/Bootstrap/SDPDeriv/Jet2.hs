{-# LANGUAGE DataKinds           #-}
{-# LANGUAGE DeriveAnyClass      #-}
{-# LANGUAGE DeriveFunctor       #-}
{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE DeriveTraversable   #-}
{-# LANGUAGE FlexibleContexts    #-}
{-# LANGUAGE FlexibleInstances   #-}
{-# LANGUAGE NamedFieldPuns      #-}
{-# LANGUAGE PolyKinds           #-}
{-# LANGUAGE RankNTypes          #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE StaticPointers      #-}
{-# LANGUAGE TypeApplications    #-}
{-# LANGUAGE TypeFamilies        #-}

module Hyperion.Bootstrap.SDPDeriv.Jet2 where

import           Bootstrap.Bounds.Crossing.UnorderedTuples (UPair, uPair)
import           Bootstrap.Math.Linear                     ((.*))
import qualified Bootstrap.Math.Linear                     as L
import           Bootstrap.Math.VectorSpace                (scale, zero, (*^))
import           Data.Aeson                                (FromJSON, ToJSON)
import           Data.Binary                               (Binary)
import qualified Data.Foldable                             as Foldable
import           Data.Functor.Identity                     (Identity (..))
import           Data.Map.Strict                           (Map)
import qualified Data.Map.Strict                           as Map
import           Data.Matrix.Static                        (Matrix)
import qualified Data.Matrix.Static                        as Matrix
import           GHC.Generics                              (Generic)
import           GHC.TypeNats                              (KnownNat)
import           Hyperion.Static                           (Dict (..),
                                                            Static (..), cAp,
                                                            cPtr)
import qualified Linear.Matrix                             as L
import           Linear.V                                  (V)
import           Linear.Vector                             ((^/))
import qualified Numeric.AD                                as AD
import           Type.Reflection                           (Typeable)

-- | The Taylor expansion of a function, truncated to quadratic order.
data Jet2 n a = MkJet2
  { constant :: a
  , gradient :: V n a
  , hessian  :: Matrix n n a -- ^ Should be symmetric
  } deriving (Eq, Ord, Show, Generic, Binary, FromJSON, ToJSON)

instance (KnownNat n, Typeable a, Static (Binary a)) => Static (Binary (Jet2 n a)) where
  closureDict = cPtr (static (\Dict Dict -> Dict)) `cAp`
                closureDict @(KnownNat n) `cAp`
                closureDict @(Binary a)

-- | Evaluate the Jet2 on a vector 'x'
eval :: (KnownNat n, Fractional a) => Jet2 n a -> V n a -> a
eval (MkJet2 c g h) x = c + g .* x + (x .* h .* x) / 2

newtype FractionalMap = MkFractionalMap (forall k . Fractional k => k -> k)

-- | Compose a twice-differentiable scalar function with a Jet2
compose :: (KnownNat n, Eq a, Fractional a) => FractionalMap -> Jet2 n a -> Jet2 n a
compose (MkFractionalMap f) (MkJet2 c g h) =
  MkJet2 f0 (f1 *^ g) (f1 *^ h + f2 *^ L.outer g g)
  where
    f0 = f c
    f1 = runIdentity $ AD.grad (f . runIdentity) (Identity c)
    f2 = runIdentity . runIdentity $ AD.hessian (f . runIdentity) (Identity c)

-- | A collection of 'a's for each vector needed to compute the
-- gradient and hessian. When 'a' is a numeric type, a 'Jet2 n a' can
-- be obtained from 'GradHessianPoints n a' together with a constant
-- value -- see 'fromPoints'. It is helpful to use 'GradHessianPoints
-- n' as a general container that can be manipulated through its
-- 'Functor' and 'Traversable' instances.
data GradHessianPoints n a = MkGradHessianPoints
  { plusBasis  :: V n a -- ^ values associated with  dx e_i for each i
  , minusBasis :: V n a -- ^ values associated with -dx e_i for each i
  , basisSums  :: Map (UPair Int) a -- ^ values associated with dx
                                    -- (e_i + e_j) for each unordered
                                    -- (i,j)
  , dx         :: Rational
  } deriving (Functor, Foldable, Traversable)

-- | GradHessianPoints containing the points themselves.
tautologyPoints :: (KnownNat n, Fractional a) => Rational -> GradHessianPoints n (V n a)
tautologyPoints dx = MkGradHessianPoints ePlus eMinus eSums dx
  where
    dx' = fromRational dx
    ePlus  = scale dx' <$> L.identity
    eMinus = negate <$> ePlus
    eSums = Map.fromList $ do
      (i, ei) <- zip [1..] (Foldable.toList ePlus)
      (j, ej) <- zip [1..] (Foldable.toList ePlus)
      pure (uPair i j, ei + ej)

-- | Compute a Jet2 from 'GradHessianPoints'. The function used to
-- generate GradHessianPoints is assumed to vanish at zero.
fromPoints :: (KnownNat n, Fractional a) => a -> GradHessianPoints n a -> Jet2 n a
fromPoints c (MkGradHessianPoints ePlus eMinus eSums dx') = MkJet2 c g h
  where
    dx = fromRational dx'
    g = (ePlus - eMinus) ^/ (2*dx)
    h = Matrix.matrix $ \(i,j) ->
      if i == j
      -- Take care that 'Matrix' elements are 1-indexed, while 'V'
      -- elements are 0-indexed.
      then (ePlus L.! (i-1) + eMinus L.! (i-1)) / (dx*dx)
      else (eSums Map.! uPair i j - ePlus L.! (i-1) - ePlus L.! (j-1)) / (dx*dx)

-- | Should evaluate to 'True' for any valid 'Jet2'. This provides a
-- strong test of 'fromPoints', 'eval', and 'tautologyPoints'.
testJet2 :: (KnownNat n, Eq a, Fractional a) => Rational -> Jet2 n a -> Bool
testJet2 dx j =
  j == fromPoints (eval j zero) (fmap (\x -> eval j x - eval j zero) (tautologyPoints dx))

newtonStep :: (KnownNat n, Fractional a) => Jet2 n a -> V n a
newtonStep j = - L.luSolve (L.toRows (hessian j)) (gradient j)
