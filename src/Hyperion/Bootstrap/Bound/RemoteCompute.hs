{-# LANGUAGE FlexibleContexts  #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE StaticPointers    #-}
{-# LANGUAGE TypeFamilies      #-}

module Hyperion.Bootstrap.Bound.RemoteCompute where

import           Control.Monad.IO.Class           (liftIO)
import           Control.Monad.Reader             (asks)
import           Data.Aeson                       (ToJSON)
import           Data.Binary                      (Binary)
import           Data.Time                        (NominalDiffTime)
import           Hyperion                         (Cluster, Dict (..), Job,
                                                   Static (..), cAp, cPure,
                                                   clusterJobOptions,
                                                   newWorkDir, ptrAp,
                                                   remoteEvalJob)
import           Hyperion.Bootstrap.Bound.Compute (computeWithFileTreatment,
                                                   continueSDPBCheckpointed,
                                                   defaultBoundFiles,
                                                   keepOutAndCheckpoint,
                                                   setFixedTimeLimit, keepAllFiles)
import           Hyperion.Bootstrap.Bound.Types   (Bound (..),
                                                   BoundFileTreatment,
                                                   BoundFiles, BuildInJob,
                                                   SDPFetchBuildConfig, ToSDP)
import qualified Hyperion.Database                as DB
import qualified Hyperion.Log                     as Log
import qualified Hyperion.Slurm                   as Slurm
import qualified SDPB
import           Type.Reflection                  (Typeable)

-- | Compute a bound, keep only the out files and the checkpoint, and
-- return the result.
remoteCompute
  :: ( Typeable b
     , Static (Show b)
     , Static (ToJSON b)
     , Static (ToSDP b)
     , Static (Binary b)
     , Static (SDPFetchBuildConfig b)
     , Static (BuildInJob b)
     )
  => Bound Int b
  -> Cluster SDPB.Output
remoteCompute =
  remoteComputeWithFileTreatment keepOutAndCheckpoint defaultBoundFiles

-- | Compute a bound, don't delete any of the files, and return the
-- result together with the 'BoundFiles'.
remoteComputeKeepFiles
  :: ( Typeable b
     , Static (Show b)
     , Static (ToJSON b)
     , Static (ToSDP b)
     , Static (Binary b)
     , Static (SDPFetchBuildConfig b)
     , Static (BuildInJob b)
     )
  => Bound Int b
  -> Cluster (SDPB.Output, BoundFiles)
remoteComputeKeepFiles =
  remoteComputeWithFileTreatment' keepAllFiles defaultBoundFiles

-- | Compute a bound with a custom 'BoundFileTreatment', returning the result
remoteComputeWithFileTreatment
  :: ( Typeable b
     , Static (Show b)
     , Static (ToJSON b)
     , Static (ToSDP b)
     , Static (Binary b)
     , Static (SDPFetchBuildConfig b)
     , Static (BuildInJob b)
     )
  => BoundFileTreatment
  -> (FilePath -> BoundFiles)
  -> Bound Int b
  -> Cluster SDPB.Output
remoteComputeWithFileTreatment treatment mkFiles bound =
  fmap fst $ remoteComputeWithFileTreatment' treatment mkFiles bound

-- | Compute a bound with a custom 'BoundFileTreatment', returning the
-- result and the 'BoundFiles'.
remoteComputeWithFileTreatment'
  :: ( Typeable b
     , Static (Show b)
     , Static (ToJSON b)
     , Static (ToSDP b)
     , Static (Binary b)
     , Static (SDPFetchBuildConfig b)
     , Static (BuildInJob b)
     )
  => BoundFileTreatment
  -> (FilePath -> BoundFiles)
  -> Bound Int b
  -> Cluster (SDPB.Output, BoundFiles)
remoteComputeWithFileTreatment' treatment mkFiles bound = do
  workDir <- newWorkDir bound
  jobTime <- asks (Slurm.time . clusterJobOptions)
  let boundFiles = mkFiles workDir
  result <-
    continueSDPBCheckpointed (workDir, solverParams bound) $
    remoteEvalJob $
    static computeWithTimeLimit `ptrAp`
    closureDict `cAp`
    cPure treatment `cAp`
    cPure jobTime `cAp`
    cPure bound `cAp`
    cPure boundFiles
  Log.info "Computed" (bound, result)
  DB.insert (DB.KeyValMap "computations") bound result
  return (result, boundFiles)

computeWithTimeLimit
  :: Dict
     ( Show b
     , Typeable b
     , ToJSON b
     , Static (ToSDP b)
     , Static (Binary b)
     , Static (SDPFetchBuildConfig b)
     , BuildInJob b
     )
  -> BoundFileTreatment
  -> NominalDiffTime
  -> Bound Int b
  -> BoundFiles
  -> Job SDPB.Output
computeWithTimeLimit Dict treatment jobTime bound files = do
  -- We ask SDPB to terminate within 90% of the jobTime. This
  -- should ensure sufficiently prompt exit as long as an
  -- iteration doesn't take 10% or more of the jobTime.
  solverParams' <- liftIO $ setFixedTimeLimit (0.9*jobTime) (solverParams bound)
  computeWithFileTreatment treatment bound { solverParams = solverParams' } files
